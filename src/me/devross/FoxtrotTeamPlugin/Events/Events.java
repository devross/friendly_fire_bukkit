package me.devross.FoxtrotTeamPlugin.Events;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class Events implements Listener{
	
	@EventHandler
	public void onDamageOpponent(EntityDamageByEntityEvent e) {
		final Arrow arrow;
		if (e.getDamager() instanceof Arrow) {
			arrow = (Arrow) e.getDamager();		
			Player damager = (Player) arrow.getShooter();
			
			if(e.getEntity().hasPermission("nofriendlyfire.monsters") && damager.hasPermission("nofriendlyfire.monsters")) {
				e.setDamage(0.0);
				e.setCancelled(true);
			}
			if(e.getEntity().hasPermission("nofriendlyfire.default") && damager.hasPermission("nofriendlyfire.default")) {
				e.setDamage(0.0);
				e.setCancelled(true);
			}
			if(e.getEntity().hasPermission("nofriendlyfire.olympus") && damager.hasPermission("nofriendlyfire.olympus")) {
				e.setDamage(0.0);
				e.setCancelled(true);
			}
		}
		
		if(!(e.getDamager() instanceof Player)) {
			return;
		}
		if(!(e.getEntity() instanceof Player)) {
			return;
		}
		if(e.getEntity().hasPermission("nofriendlyfire.monsters") && e.getDamager().hasPermission("nofriendlyfire.monsters")) {
			e.setDamage(0.0);
			e.setCancelled(true);
		}
		if(e.getEntity().hasPermission("nofriendlyfire.default") && e.getDamager().hasPermission("nofriendlyfire.default")) {
			e.setDamage(0.0);
			e.setCancelled(true);
		}
		if(e.getEntity().hasPermission("nofriendlyfire.olympus") && e.getDamager().hasPermission("nofriendlyfire.olympus")) {
			e.setDamage(0.0);
			e.setCancelled(true);
		}
	}
}
