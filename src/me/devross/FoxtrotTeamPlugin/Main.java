package me.devross.FoxtrotTeamPlugin;

import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import me.devross.FoxtrotTeamPlugin.Events.Events;

public class Main extends JavaPlugin implements Listener {
	
	public void onEnable() {
		getServer().getConsoleSender().sendMessage("Hello Jacktar");
		getServer().getPluginManager().registerEvents(new Events(), this);
	}
	
	public void onDisable() {
		System.out.println("[FOXTROT PLUGIN] Teams -> Disabled");
	}
	
}
